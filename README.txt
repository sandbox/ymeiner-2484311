-- SUMMARY --

JSON filter takes JSON object data stored in a field
presents it in a readable form as a filter for a field.

The field can be one of the three types:
  'text',
  'text_long',
  'text_with_summary'.

There are two presentation options:

1. Unordered List
2. Table with nested unordered list for values.

The presentation fits a tree structure for the json
and not limited to a flat table.


-- REQUIREMENTS --

None.


-- INSTALLATION --

Download module, place it in the modules folder (/sites/all/modules)
and enable it
  Administration menu.


-- CONFIGURATION --

The filter is just another filter in the display settings of the field.
For example, in order to change a field display for a node filed,
please do the following:

1. Go to Manage Display (/admin/structure/types/manage/{TYPE}/display)
2. Change the Format to Readable JSON
   (note that this only works on fields of types 'text', 'text_long'
   and 'text_with_summary').
3. Click on the gear (edit settings)
4. Chose your presentation type (default - Unordered list) and click update
5. Click on save.



-- CUSTOMIZATION --

No customizations for this module


-- TROUBLESHOOTING --

* If filter does not appear in the filters list, please make sure that it is enabled.

* In order to use this module with solr seach results, consider adding Design suit (http://drupal.org/node/644662) and you will be able to chose the filter form the configuration page.

-- FAQ --

* no faq, as questions will raize from the issues, we will add questions.


-- CONTACT --

Current maintainers:
* Yaron Meiner - https://www.drupal.org/u/ymeiner
* Denis Voitenko (scripthead) - https://www.drupal.org/u/scripthead

Big thanks to tikaszvince and Ayesh on the support and for pushign this module to pro-level.
